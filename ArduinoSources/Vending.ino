#include <Servo.h>
Servo firstLeftServo;
Servo firstSecondServo;
Servo secondtLeftServo;
Servo secondRightServo;

const int stepPin = 3;
const int dirPin = 4;

const int ms1Pin = 5;
const int ms2Pin = 6;
const int ms3Pin = 7;

void setup()
{
  Serial.begin(9600); 
  Serial1.begin(9600);
  
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);

  pinMode(ms1Pin, OUTPUT);
  pinMode(ms2Pin, OUTPUT);
  pinMode(ms3Pin, OUTPUT);
  
  firstLeftServo.attach(11);
  firstSecondServo.attach(12);
  //secondtLeftServo.attach(13); 
  //secondRightServo.attach(14);
}

void upperStepperMoveUp()
{
  digitalWrite(ms2Pin, HIGH);
 
  digitalWrite(dirPin, HIGH);
  for(int i =0; i < 3200; i++)
  {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(500);
  }
}

void upperStepperMoveDown()
{
  digitalWrite(ms2Pin, HIGH);

  digitalWrite(dirPin, LOW);
  for(int i =0; i < 3200; i++)
  {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(500);
  }
}

void loop()
{

  if(Serial1.available ()>0)
  {
    int buffer_value = Serial1.read();

    Serial.println(buffer_value);
    if(buffer_value == '1')
    {
        firstLeftServo.write(16);
        delay(2000);
    }
    else if(buffer_value == '2')
    {
        firstSecondServo.write(96);
        delay(2000);
    }
    else if(buffer_value == '3')
    {
       //secondtLeftServo.write(96);
       upperStepperMoveUp();
       delay(2000);
    }
    else if(buffer_value == '4')
    {
       //secondRightServo.write(96);
       upperStepperMoveDown();
       delay(2000);
    }
  }
}
