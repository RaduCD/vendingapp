const int stepPin = 9;
const int dirPin = 10;

const int ms1Pin = 11;
const int ms2Pin = 12;
const int ms3Pin = 13;

void setup() {
 pinMode(stepPin, OUTPUT);
 pinMode(dirPin, OUTPUT);

 pinMode(ms1Pin, OUTPUT);
 pinMode(ms2Pin, OUTPUT);
 pinMode(ms3Pin, OUTPUT);
}

void loop() {

  digitalWrite(ms2Pin, HIGH);
 
   digitalWrite(dirPin, HIGH);
  for(int i =0; i < 3200; i++)
  {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(500);
  }

  delay(1000);

  digitalWrite(dirPin, LOW);
  for(int i =0; i < 3200; i++)
  {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(500);
  }

  delay(2000);
  digitalWrite(ms2Pin, LOW);
}
