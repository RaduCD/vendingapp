void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); //Tx0 and Rx0  //Set Baud Rate to 9600 for Serial Communication Tx0 and Rx0
  Serial1.begin(9600);  //Tx1 and Rx1  //Connected to Bluetooth Module HC-05 (Bluetooth 2.0)
  //Serial2;  //Tx2 and Rx2
  //Serial3;  //Tx3 and Rx3

  pinMode(13, OUTPUT);  //Set Pin 13 as Output (Connected to LED)

  Serial.println("press 1/0 to switch ON/OFF the LED");  //To Desktop
  Serial1.println("press 1/0 to switch ON/OFF the LED"); //To mobile
}

void loop() {
  // put your main code here, to run repeatedly:
  
  if(Serial1.available ()>0)
  {
    int buffer_value = Serial1.read();

    Serial.println(buffer_value);
    if(buffer_value == '1')
    {
      digitalWrite(13, HIGH);    //Turn ON LED
      Serial.println("LED ON");  //Arduino Terminal of Desktop 
      Serial1.println("+"); //Bluetooth Terminal on Mobile
    }
    else if(buffer_value == '0')
    {
      digitalWrite(13, LOW);      //Turn OFF LED
      Serial.println("LED OFF");  //Arduino Terminal on Desktop
      Serial1.println('-'); //Bluetooth Terminal on Mobile 
    }
  }
}
