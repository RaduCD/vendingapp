package com.example.vendingapp.model;

public class Purchase {

    public Purchase(){
    }

    private String productId;
    private String date;

    public Purchase(String productId, String date) {
        this.productId = productId;
        this.date = date;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
