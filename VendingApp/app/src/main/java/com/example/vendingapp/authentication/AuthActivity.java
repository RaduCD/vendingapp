package com.example.vendingapp.authentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.vendingapp.R;
import com.example.vendingapp.authentication.LoginFragment;
import com.example.vendingapp.payment.PaymentActivity;
import com.example.vendingapp.product.CacheProduct;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Logger;

import java.io.IOException;
import java.lang.ref.WeakReference;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AuthActivity extends AppCompatActivity {

    private static final String BACKEND_URL = "https://stormy-brushlands-53643.herokuapp.com/";
    private OkHttpClient httpClient = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        FirebaseDatabase.getInstance().setLogLevel(Logger.Level.DEBUG);

        CacheProduct.init();

        sendCheckIdleRequest();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.auth_fragment_container, new LoginFragment())
                .commit();
    }

    private void sendCheckIdleRequest() {

        Request request = new Request.Builder()
                .url(BACKEND_URL + "check-idle-state")
                .build();

        httpClient.newCall(request).enqueue(new CheckIdleCallback(this));
    }




    private static final class CheckIdleCallback implements Callback {
        @NonNull
        private final WeakReference<AuthActivity> activityRef;

        CheckIdleCallback(@NonNull AuthActivity activity) {
            activityRef = new WeakReference<>(activity);
        }

        @Override
        public void onFailure(@NonNull Call call, @NonNull IOException e) {
            final AuthActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            Log.e("CHECK IDLE STATE", e.getMessage());
            activity.runOnUiThread(() ->
                    Toast.makeText(
                            activity, "Error: CHECK IDLE STATE" + e.toString(), Toast.LENGTH_LONG
                    ).show()
            );
        }

        @Override
        public void onResponse(@NonNull Call call, @NonNull final Response response)
                throws IOException {
            final AuthActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            if (!response.isSuccessful()) {
                activity.runOnUiThread(() ->
                        Toast.makeText(
                                activity, "Error: onResponse" + response.toString(), Toast.LENGTH_LONG
                        ).show()
                );
                Log.e("CHECK IDLE STATE", response.toString());

            } else {
                Log.d("CHECK IDLE STATE", "OK");
            }
        }
    }
}
