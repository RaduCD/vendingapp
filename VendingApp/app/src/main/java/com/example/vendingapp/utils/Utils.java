package com.example.vendingapp.utils;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.example.vendingapp.R;
import com.example.vendingapp.authentication.RegisterFragment;
import com.example.vendingapp.product.ProductsPageFragment;

public class Utils {

    public static final String EXTRA_TEXT = "text";

    public static void goToFragmentFromFragment(FragmentActivity fragmentActivity, Fragment fragmentTo, String backStackMessage){
        fragmentActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.auth_fragment_container, fragmentTo)
                .addToBackStack(backStackMessage)
                .commit();
    }

    public static Bundle getBundle(String text){
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_TEXT, text);
        return bundle;
    }

    public static void goToProductsFragmentFromHomeActivity(FragmentActivity fragmentActivity, Fragment fragmentTo, Bundle args){
             fragmentTo.setArguments(args);
            fragmentActivity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.id_home_container, fragmentTo)
                    .commit();
    }

    public static void goToPurchasesFragmentFromHomeFragment(FragmentActivity fragmentActivity, Fragment fragmentTo, String backStackMessage){
        fragmentActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.id_home_container, fragmentTo)
                .addToBackStack(backStackMessage)
                .commit();
    }
}
