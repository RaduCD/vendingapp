package com.example.vendingapp.manager;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.vendingapp.R;
import com.example.vendingapp.model.Product;
import com.example.vendingapp.model.Purchase;
import com.example.vendingapp.product.CacheProduct;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.InterruptedIOException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class DatabaseManager {

    //private FirebaseAuth fireBaseAuth;
    private FirebaseDatabase database;
    private DatabaseReference databaseReference;

    private String userId;

    public DatabaseManager(){
        database = FirebaseDatabase.getInstance();

       // userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        userId = "nxpraaJxcEOhVCDTirl1cvyfgkJ2";
    }

    public void seedProducts(String referenceTable){
        databaseReference = database.getReference(referenceTable);

        Product healthProtein = new Product("Health Protein", "", "3$", R.drawable.health_protein);
        databaseReference.child("first_item").setValue(healthProtein);

        Product proteinBar =  new Product("Protein Bar", "", "2$", R.drawable.protein_bar);
        databaseReference.child("second_item").setValue(proteinBar);

        Product kitKat = new Product("Kit Kat", "", "1$", R.drawable.kit_kat);
        databaseReference.child("third_item").setValue(kitKat);

        Product roshen = new Product("Roshen", "", "3$", R.drawable.roshen);
        databaseReference.child("fourth_item").setValue(roshen);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void addPurchase(Product product){

        databaseReference = database.getReference("purchases");

        Purchase purchase = new Purchase();
        String productKey = CacheProduct.getProductKey(product.getName());

        purchase.setProductId(productKey);
        purchase.setDate(OffsetDateTime.now().toString());

        databaseReference.child(userId).push().setValue(purchase);
    }
}

