package com.example.vendingapp.home;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.vendingapp.R;
import com.example.vendingapp.authentication.LoginFragment;
import com.example.vendingapp.bluetooth.DeviceList;
import com.example.vendingapp.home.menu.DrawerAdapter;
import com.example.vendingapp.home.menu.DrawerItem;
import com.example.vendingapp.home.menu.SimpleItem;
import com.example.vendingapp.home.menu.SpaceItem;
import com.example.vendingapp.manager.BluetoothManager;
import com.example.vendingapp.product.ProductsPageFragment;
import com.example.vendingapp.product.PurchaseHistoryFragrament;
import com.example.vendingapp.utils.Utils;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import java.util.Arrays;

import okhttp3.internal.Util;


public class HomeActivity extends AppCompatActivity implements DrawerAdapter.OnItemSelectedListener{


    private static final int POS_DASHBOARD = 0;
    private static final int POS_ACCOUNT = 1;
    private static final int POS_MESSAGES = 2;
    private static final int POS_CART = 3;
    private static final int POS_LOGOUT = 5;

    private String[] screenTitles;
    private Drawable[] screenIcons;

    private SlidingRootNav slidingRootNav;

    private ImageView bluetooth_logo;
    private BluetoothManager bluetoothManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_layout);
        setSupportActionBar(toolbar);*/
        bluetoothManager = BluetoothManager.getInstance();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Resources resources = getApplicationContext().getResources();
        int disconnectedColor = resources.getColor(R.color.design_default_color_error);
        int connectedColor = resources.getColor(R.color.blue);

        bluetooth_logo = (ImageView) findViewById(R.id.bluetoothLogo);
        bluetooth_logo.setClickable(true);

        if(bluetoothManager.getConnected())
            bluetooth_logo.setColorFilter(connectedColor);
        else
            bluetooth_logo.setColorFilter(disconnectedColor);

        bluetooth_logo.setOnClickListener(this::goToDevicesList);


        slidingRootNav = new SlidingRootNavBuilder(this)
                .withToolbarMenuToggle(toolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.menu_left_drawer)
                .inject();

        screenIcons = loadScreenIcons();
        screenTitles = loadScreenTitles();

        DrawerAdapter adapter = new DrawerAdapter(Arrays.asList(
                createItemFor(POS_DASHBOARD).setChecked(true),
                createItemFor(POS_ACCOUNT),
                createItemFor(POS_MESSAGES),
                createItemFor(POS_CART),
                new SpaceItem(48),
                createItemFor(POS_LOGOUT)));

        adapter.setListener(this);

        RecyclerView list = findViewById(R.id.list);
        list.setNestedScrollingEnabled(false);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);

        adapter.setSelected(POS_DASHBOARD);
    }

  /*  @Override
    public void onContentChanged() {
        super.onContentChanged();
    }*/

    private void goToDevicesList(View view) {
        Intent intent = new Intent(this, DeviceList.class);
        startActivity(intent);
    }

    @Override
    public void onItemSelected(int position) {
        if (position == POS_LOGOUT) {
            finish();
        }
        else if (position == POS_MESSAGES){
            Utils.goToPurchasesFragmentFromHomeFragment(this, new PurchaseHistoryFragrament(), "PURCHASES HISTORY");
        }
        else {
            Bundle bundle = Utils.getBundle(screenTitles[position]);
            Utils.goToProductsFragmentFromHomeActivity(this, new ProductsPageFragment(), bundle);
        }

        slidingRootNav.closeMenu();

        //Fragment selectedScreen = ProductsPageFragment.createFor(screenTitles[position]);
       // showFragment(selectedScreen);
    }

    /*private void showFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.id_home_container, fragment)
                .commit();
    }*/

    @SuppressWarnings("rawtypes")
    private DrawerItem createItemFor(int position) {
        return new SimpleItem(screenIcons[position], screenTitles[position])
                .withIconTint(color(R.color.design_default_color_secondary))
                .withTextTint(color(R.color.design_default_color_primary))
                .withSelectedIconTint(color(R.color.blue))
                .withSelectedTextTint(color(R.color.common_google_signin_btn_text_light_default));
    }

    private String[] loadScreenTitles() {
        return getResources().getStringArray(R.array.id_menu_array_titles);
    }

    private Drawable[] loadScreenIcons() {
        TypedArray ta = getResources().obtainTypedArray(R.array.id_menu_array_logos);
        Drawable[] icons = new Drawable[ta.length()];
        for (int index = 0; index < ta.length(); index++) {
            int id = ta.getResourceId(index, 0);
            if (id != 0) {
                icons[index] = ContextCompat.getDrawable(this, id);
            }
        }
        ta.recycle();
        return icons;
    }

    @ColorInt
    private int color(@ColorRes int res) {
        return ContextCompat.getColor(this, res);
    }

}

