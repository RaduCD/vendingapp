package com.example.vendingapp.authentication;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.vendingapp.R;

public class RegisterFragment extends Fragment implements View.OnClickListener {

    Button btn_next;
    EditText et_firstName;
    EditText et_lastName;
    EditText et_email;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register, container, false);

        et_firstName = view.findViewById(R.id.id_password_register_et);
        et_lastName = view.findViewById(R.id.id_last_name_ed);
        et_email = view.findViewById(R.id.id_email_register_ed);
        btn_next = view.findViewById(R.id.id_next_btn);

        setAllListeners();
        return view;
    }

    private void setAllListeners() {
        btn_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId) {
            case R.id.id_next_btn:
                goToRegisterFragmentGenderDOB(getRegisterFragmentDateAndGender());
                break;
        }
    }

    private void goToRegisterFragmentGenderDOB(RegisterGenderDOBFragment genderDOBFragment){
        if(genderDOBFragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.auth_fragment_container, genderDOBFragment)
                    .addToBackStack("REGISTER GENDER DOB FRAGMENT")
                    .commit();
        }
    }

    private RegisterGenderDOBFragment getRegisterFragmentDateAndGender(){
        if(isValidFields())
        {
            String firstNameValue = et_firstName.getText().toString().trim();
            String lastNameValue =  et_lastName.getText().toString().trim();
            String emailValue = et_email.getText().toString().trim();

            Bundle bundle = new Bundle();
            bundle.putString("firstName", firstNameValue);
            bundle.putString("lastName", lastNameValue);
            bundle.putString("email", emailValue);

            RegisterGenderDOBFragment registerFragmentGenderDOB = new RegisterGenderDOBFragment();
            registerFragmentGenderDOB.setArguments(bundle);

            return registerFragmentGenderDOB;
        }
        return null;
    }

    private boolean isValidFields() {
        String firstNameValue = et_firstName.getText().toString();
        String lastNameValue =  et_lastName.getText().toString();
        String emailValue = et_email.getText().toString();

        if(firstNameValue.isEmpty()) {
            et_firstName.setError("Please enter your first name");
            et_firstName.requestFocus();
        }
        else if(lastNameValue.isEmpty()) {
            et_lastName.setError("Please enter your last name");
            et_lastName.requestFocus();
        }
        else if(emailValue.isEmpty()) {
            et_email.setError("Please enter your email");
            et_email.requestFocus();
        }
        else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(emailValue).matches()){
            et_email.setError("Invalid email structure");
            et_email.requestFocus();
            et_email.setText("");
        }
        else return true;
        return false;
    }
}