package com.example.vendingapp.product;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vendingapp.R;
import com.example.vendingapp.model.Product;
import com.example.vendingapp.model.Purchase;

import java.util.List;

public class PurchasesAdapter extends RecyclerView.Adapter<PurchasesAdapter.MyViewHolder> {

    private List<Purchase> purchases;

    public PurchasesAdapter(List<Purchase> purchases) {
        this.purchases = purchases;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.purchase_history_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Purchase purchase = purchases.get(position);

        Product product = CacheProduct.getProduct(purchase.getProductId());

        holder.image.setImageResource(product.getPhoto());
        holder.title.setText(product.getName());
        holder.price.setText(product.getPrice());
        holder.date.setText(purchase.getDate());
    }

    @Override
    public int getItemCount() {
        return purchases.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

       private ImageView image;
       private TextView title;
       private TextView price;
       private TextView date;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.id_purchase_row_img);
            title = itemView.findViewById(R.id.id_purchase_row_item_title);
            price = itemView.findViewById(R.id.id_purchase_row_item_price);
            date = itemView.findViewById(R.id.id_purchase_row_item_date);
        }
    }

}
