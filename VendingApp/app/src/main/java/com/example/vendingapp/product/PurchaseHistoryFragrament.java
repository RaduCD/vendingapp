package com.example.vendingapp.product;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vendingapp.R;
import com.example.vendingapp.manager.DatabaseManager;
import com.example.vendingapp.model.Purchase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;


public class PurchaseHistoryFragrament extends Fragment {

    private RecyclerView recyclerView;
    private PurchasesAdapter purchasesAdapter;
    private List<Purchase> purchases;

    //private FirebaseAuth fireBaseAuth;
    private DatabaseReference databaseReference;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View view = inflater.inflate(R.layout.fragment_purchase_history_fragrament, container, false);

        recyclerView = view.findViewById(R.id.id_purchase_history_recycler_view);
        purchases = new ArrayList<Purchase>();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        //String userId = fireBaseAuth.getCurrentUser().getUid();
        String userId = "nxpraaJxcEOhVCDTirl1cvyfgkJ2";
        databaseReference = FirebaseDatabase.getInstance().getReference("purchases").child(userId);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    Purchase purchase = dataSnapshot.getValue(Purchase.class);

                    String rawDate = purchase.getDate();
                    String shortDate = getShortDate(rawDate);
                    purchase.setDate(shortDate);

                    purchases.add(purchase);
                }
                purchasesAdapter = new PurchasesAdapter(purchases);
                recyclerView.setAdapter(purchasesAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

       return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String getShortDate(String longDate) {
        StringBuilder result = new StringBuilder();
        OffsetDateTime date =  OffsetDateTime.parse(longDate);

         result.append(date.getDayOfMonth());
         result.append('-');
         result.append(date.getMonthValue());
         result.append('-');
         result.append(date.getYear());
         result.append("  ");

        result.append(date.getHour());
        result.append(':');
        result.append(date.getMinute());
        result.append(':');
        result.append(date.getSecond());

        return result.toString();
    }
}