package com.example.vendingapp.product;
import com.example.vendingapp.R;
import com.example.vendingapp.model.Product;

import java.util.ArrayList;
import java.util.List;

public class CacheProduct {

    public static List<ProductDb> products = new ArrayList<ProductDb>();

    public static void init(){

        Product healthProtein = new Product("Health Protein", "", "3$", R.drawable.health_protein);
        ProductDb healthProteinDb = new ProductDb("first_item", healthProtein);
        products.add(healthProteinDb);

        Product proteinBar =  new Product("Protein Bar", "", "2$", R.drawable.protein_bar);
        ProductDb proteinBarDb = new ProductDb("second_item", proteinBar);
        products.add(proteinBarDb);

        Product kitKat = new Product("Kit Kat", "", "1$", R.drawable.kit_kat);
        ProductDb kitKatDb = new ProductDb("third_item", kitKat);
        products.add(kitKatDb);

        Product roshen = new Product("Roshen", "", "3$", R.drawable.roshen);
        ProductDb roshenDb = new ProductDb("fourth_item", roshen);
        products.add(roshenDb);
    }


    public static Product getProduct(String key){
        for (ProductDb productDb : products){
            if(productDb.getId().equals(key))
                return productDb.getProduct();
        }
        return null;
    }

    public static String getProductKey(String productName){
        for (ProductDb productDb : products){
            if(productDb.getProduct().getName().equals(productName))
                return productDb.getId();
        }
        return null;
    }

}
 class ProductDb {
    private String id;
    private Product product;

    public ProductDb(String id, Product product) {
        this.id = id;
        this.product = product;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
