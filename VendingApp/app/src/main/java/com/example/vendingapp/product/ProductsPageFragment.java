package com.example.vendingapp.product;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vendingapp.R;
import com.example.vendingapp.bluetooth.DeviceList;
import com.example.vendingapp.manager.BluetoothManager;
import com.example.vendingapp.model.Product;
import com.example.vendingapp.payment.PaymentActivity;
import com.example.vendingapp.utils.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;

public class ProductsPageFragment extends Fragment {

    private ImageView health_protein_img;
    private ImageView protein_bar_img;
    private ImageView kit_kat_img;
    private ImageView roshen_img;

    private TextView health_protein_tv;
    private TextView protein_bar_tv;
    private TextView kit_kat_tv;
    private TextView roshen_tv;

    private BluetoothManager bluetoothManager;

    private FirebaseAuth mAuth;
    private DatabaseReference databaseReference;

    private String currentUserId;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        bluetoothManager = BluetoothManager.getInstance();

        mAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        //currentUserId = mAuth.getCurrentUser().getUid();

        View view = inflater.inflate(R.layout.fragment_products, container, false);

        health_protein_img = view.findViewById(R.id.id_img_health_protein);
        protein_bar_img = view.findViewById(R.id.id_img_protein_bar);
        kit_kat_img = view.findViewById(R.id.id_img_kit_kat);
        roshen_img = view.findViewById(R.id.id_img_roshen);

        health_protein_tv = view.findViewById(R.id.id_price_health_protein);
        protein_bar_tv = view.findViewById(R.id.id_price_protein_bar);
        kit_kat_tv = view.findViewById(R.id.id_price_kit_kat);
        roshen_tv = view.findViewById(R.id.id_price_roshen);

        setOnClickListenerItems();

        return view;
    }


    private void setOnClickListenerItems(){
         health_protein_img.setOnClickListener(this::buyHealthProtein);
         protein_bar_img.setOnClickListener(this::buyProteinBar);
         kit_kat_img.setOnClickListener(this::buyKitKat);
         roshen_img.setOnClickListener(this::buyRoshen);
    }

    private void buyHealthProtein(View view){

        Product product = CacheProduct.getProduct("first_item");

        Intent intent = getPurchaseIntent(product);
        startActivity(intent);
    }

    private void buyProteinBar(View view) {
        Product product = CacheProduct.getProduct("second_item");

        Intent intent = getPurchaseIntent(product);
        startActivity(intent);
    }

    private void buyKitKat(View view) {
        Product product = CacheProduct.getProduct("third_item");

        Intent intent = getPurchaseIntent(product);
        startActivity(intent);
    }

    private void buyRoshen(View view) {
        Product product = CacheProduct.getProduct("fourth_item");

        Intent intent = getPurchaseIntent(product);
        startActivity(intent);
    }

    private Intent getPurchaseIntent(Product product){
        Intent intent = null;
        //todo uncomment the code after connecting with arduino
        //if(bluetoothManager.getConnected())
            intent = new Intent(getActivity(), PaymentActivity.class);
       // else
         //   intent = new Intent(getActivity(), DeviceList.class);

        intent.putExtra("Product", product);

        return intent;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
       //super.onViewCreated(view, savedInstanceState);
        Bundle args = getArguments();
        final String text = args != null ? args.getString(Utils.EXTRA_TEXT) : "";
        TextView textView = view.findViewById(R.id.text_prod_fragm);
        textView.setText(text);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Toast.makeText(v.getContext(), text, Toast.LENGTH_SHORT).show();
            }
        });
    }
}