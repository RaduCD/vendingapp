package com.example.vendingapp.model;

import java.io.Serializable;

public class Product implements Serializable {

    public Product(){
    }

    public Product(String name, String descripton, String price, int photo) {
        this.name = name;
        this.descripton = descripton;
        this.price = price;
        this.photo = photo;
    }

    private String  id;;
    private String name;
    private String descripton;
    private String price;
    private int photo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescripton() {
        return descripton;
    }

    public void setDescripton(String descripton) {
        this.descripton = descripton;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
