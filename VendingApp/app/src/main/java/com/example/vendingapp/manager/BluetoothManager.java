package com.example.vendingapp.manager;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.util.UUID;

public class BluetoothManager {

    private BluetoothAdapter bluetoothAdapter = null;
    private BluetoothSocket bluetoothSocket = null;

    private Boolean isConnected = false;

    private String address = null;

    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private static BluetoothManager bluetoothManager;

    private BluetoothManager(){
    }

    public static BluetoothManager getInstance(){
        if(bluetoothManager == null)
            bluetoothManager = new BluetoothManager();

        return bluetoothManager;
    }

    public void Connect() throws IOException {

        updateConnectionStatus();
        if(bluetoothSocket == null){

            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
            BluetoothDevice remoteDevice = bluetoothAdapter.getRemoteDevice(address); //connects to the device's address and checks if it's available
            bluetoothSocket = remoteDevice.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
            BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
            bluetoothSocket.connect();
            isConnected = bluetoothSocket.isConnected();
        }
    }

    private void updateConnectionStatus(){
        if(bluetoothSocket != null)
            isConnected = bluetoothSocket.isConnected();
        if(!isConnected)
            bluetoothSocket = null;
    }

    public void Disconnect() throws IOException {
        if (bluetoothSocket != null) {

            bluetoothSocket.close();
            isConnected = false;
            bluetoothSocket = null;
        }
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BluetoothAdapter getBluetoothAdapter() {
        return bluetoothAdapter;
    }

    public BluetoothSocket getBluetoothSocket() {
        return bluetoothSocket;
    }

    public Boolean getConnected() {
        updateConnectionStatus();
        return isConnected;
    }

    public void setConnected(Boolean connected) {
        isConnected = connected;
    }
}
