package com.example.vendingapp.model;

public class User {

    public User(){

    }

    public User(String email, String firstName, String lastName, String date, String gender) {

        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.date = date;
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    private String email;
    private String firstName;
    private String lastName;
    private String date;
    private String gender;


}
