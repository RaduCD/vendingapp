package com.example.vendingapp.authentication;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.vendingapp.R;
import com.example.vendingapp.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterPasswordFragment extends Fragment {

    private EditText password_et;
    private EditText password_repeated_et;

    private Button register_btn;
    private ProgressBar progressBar;

    private FirebaseAuth mAuth;
    private DatabaseReference database;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register_password, container, false);

        password_et = view.findViewById(R.id.id_password_register_et);
        password_repeated_et = view.findViewById(R.id.id_repeat_password_register_et);

        register_btn = view.findViewById(R.id.id_register_btn);
        progressBar = view.findViewById(R.id.id_progressBar_register);

        register_btn.setOnClickListener(this::createUser);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference();
        return view;
    }

    private void createUser(View view){
        if(verifyRegisterFields()){

        progressBar.setVisibility(View.VISIBLE);
        User user = getUserFromArguments();
        String password = password_et.getText().toString().trim();

        mAuth.createUserWithEmailAndPassword(user.getEmail(), password)
             .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                 @Override
                 public void onComplete(@NonNull Task<AuthResult> task) {
                     if(task.isSuccessful()){

                         String uid = mAuth.getCurrentUser().getUid();
                         database.child("Users").child(uid).setValue(user)
                         .addOnCompleteListener(new OnCompleteListener<Void>() {
                             @Override
                             public void onComplete(@NonNull Task<Void> task) {
                                 if(task.isSuccessful()){
                                     Toast.makeText(getContext(), "Successfully registered", Toast.LENGTH_SHORT).show();
                                 }
                                 else{
                                     Toast.makeText(getContext(), "Can't add the user entity in database", Toast.LENGTH_SHORT).show();
                                 }
                             }
                         });
                     }
                     else
                         try {
                             throw task.getException();
                         }
                         catch(FirebaseAuthWeakPasswordException e) {
                             password_et.setError("This password is too weak");
                             password_et.requestFocus();
                             password_et.setText("");
                             password_et.setText("");
                         }
                         catch(FirebaseAuthUserCollisionException e) {
                             Toast.makeText(getContext(), "An user with this email already exists", Toast.LENGTH_SHORT).show();
                         }
                         catch (Exception e) {
                             Toast.makeText(getContext(), "Registration failed", Toast.LENGTH_SHORT).show();
                         }
                     progressBar.setVisibility(View.GONE);
                 }
             });
        }
    }

    private User getUserFromArguments(){
        String firstName = getArguments().getString("firstName", "");
        String lastName = getArguments().getString("lastName", "");
        String dob = getArguments().getString("dob", "");
        String gender = getArguments().getString("gender", "");
        String email = getArguments().getString("email", "");

        return new User(email, firstName, lastName, dob, gender);
    }

    private boolean verifyRegisterFields(){

        String passwordValue  = "";
        String passwordRepeatedValue= "";

        if(password_et != null && password_repeated_et!= null){
            passwordValue = password_et.getText().toString().trim();
            passwordRepeatedValue = password_repeated_et.getText().toString().trim();
        }
        if(passwordValue.isEmpty()){
            password_et.setError("Please type an password");
            password_et.requestFocus();
            return false;
        }
        else if(passwordValue.length() < 6){
            password_et.setError("The password must contain at least 6 characters");
            password_et.requestFocus();
            return false;
        }
        else if(passwordRepeatedValue.isEmpty()){
            password_repeated_et.setError("Please retype the password");
            password_repeated_et.requestFocus();
            return false;
        }
        else if(!passwordRepeatedValue.equals(passwordValue)){
            password_repeated_et.setError("The password does not match");
            password_repeated_et.requestFocus();
            return false;
        }
        return true;
    }
}

