package com.example.vendingapp.authentication;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.example.vendingapp.R;

import java.util.Calendar;

public class RegisterGenderDOBFragment extends Fragment {

    EditText dateOfBith_ed;
    EditText gender_ed;

    Button next_btn;

    Integer position = 0;
    String[] gender;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_register_gender_d_o_b, container, false);

       dateOfBith_ed = view.findViewById(R.id.id_dateOfBirth_ed);
       gender_ed = view.findViewById(R.id.id_gender_ed);
       next_btn = view.findViewById(R.id.id_next_gender_dob_btn);

       setAllListeners();
       return view;
    }

    private void setAllListeners() {
        dateOfBith_ed.setOnClickListener(this::getCalendarFilled);
        gender_ed.setOnClickListener(this::getGenderFilled);
        next_btn.setOnClickListener(this::goToPasswordFragment);
    }

    private void getCalendarFilled(View view){
        Calendar newCalendar = Calendar.getInstance();

        new DatePickerDialog(getContext(), (view1, year, monthOfYear, dayOfMonth) -> {
            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);

            String dateContent = new StringBuilder().append(dayOfMonth)
                    .append(".").append(monthOfYear)
                    .append(".").append(year).toString();

            dateOfBith_ed.setText(dateContent);
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    private void getGenderFilled(View view){

        AlertDialog.Builder builder  = new AlertDialog.Builder(getContext());
        gender = getResources().getStringArray(R.array.gender);
        builder.setCancelable(true);
        builder.setTitle("Select your gender") ;

        builder.setSingleChoiceItems(gender, 0,  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int pos) {
                position = pos;
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                gender_ed.setText(gender[position]);
            }
        });
        builder.show();
    }

    private void goToPasswordFragment(View view){
        if(isValidFields())
        {
            String genderValue = gender_ed.getText().toString();
            String dobValue =  dateOfBith_ed.getText().toString();

            Bundle bundle = this.getArguments();
            bundle.putString("gender", genderValue);
            bundle.putString("dob", dobValue);

            RegisterPasswordFragment registerPasswordFragment = new RegisterPasswordFragment();
            registerPasswordFragment.setArguments(bundle);

            goToPasswordFragment(registerPasswordFragment);
        }

    }

    private boolean isValidFields() {
        String genderValue = gender_ed.getText().toString();
        String dobValue =  dateOfBith_ed.getText().toString();

        if(genderValue.isEmpty()) {
            gender_ed.setError("Please select your gender");
            gender_ed.requestFocus();
        }
        else if(dobValue.isEmpty()) {
            dateOfBith_ed.setError("Please enter your last name");
            dateOfBith_ed.requestFocus();
        }

        else return true;
        return false;
    }


    private void goToPasswordFragment(RegisterPasswordFragment passwordFragment){
        if(passwordFragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.auth_fragment_container, passwordFragment)
                    .addToBackStack("REGISTER PASSWORD FRAGMENT")
                    .commit();
        }
    }
}