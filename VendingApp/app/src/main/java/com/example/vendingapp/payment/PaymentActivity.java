
   package com.example.vendingapp.payment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

import com.example.vendingapp.R;
import com.example.vendingapp.manager.DatabaseManager;
import com.example.vendingapp.model.Product;
import com.example.vendingapp.model.Purchase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PaymentActivity extends AppCompatActivity {

    // 10.0.2.2 is the Android emulator's alias to localhost
    //private static final String BACKEND_URL = "http://10.0.2.2:3000/";
    private static final String BACKEND_URL = "https://stormy-brushlands-53643.herokuapp.com/";
    private final String STRIPE_PUBLIC_KEY = "pk_test_51IcCeGKPwSLwVodVKNgPpqstpUNTKeXEim38tVI9TS2FCagQ15Q6PQjNlUTSwoT7MiS69X7deqHCGleVMkX3hES9002PHhzbbS";

    private OkHttpClient httpClient = new OkHttpClient();
    private String paymentIntentClientSecret;
    private Stripe stripe;

    Button payButton;

    private static Product product = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);



        product = (Product) getIntent().getSerializableExtra("Product");
        // Configure the SDK with your Stripe publishable key so it can make requests to Stripe
        stripe = new Stripe(
                getApplicationContext(),
                Objects.requireNonNull(STRIPE_PUBLIC_KEY)
        );

        String rawPrice = getRawPriceValue(product.getPrice());

        int price = Integer.parseInt(rawPrice);
        startCheckout(price);
    }

    private String getRawPriceValue(String price){
        StringBuffer stringBuffer= new StringBuffer(price);
        stringBuffer.deleteCharAt(stringBuffer.length()-1);
        String rawPrice = stringBuffer.toString();

        return rawPrice;
    }

    private void startCheckout(int price) {
        // Create a PaymentIntent by calling the server's endpoint.
        MediaType mediaType = MediaType.get("application/json; charset=utf-8");

        price = price * 100;

        JSONObject json = new JSONObject();
        try {
            json.put("currency", "usd");
            json.put("price", price);
        } catch (JSONException e) {

            Log.e("ERROR", e.getMessage());
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }

        String jsonMessage = json.toString();

        RequestBody body = RequestBody.create(jsonMessage, mediaType);
        Request request = new Request.Builder()
                .url(BACKEND_URL + "create-payment-intent")
                .post(body)
                .build();

        httpClient.newCall(request).enqueue(new PaymentIntentCallback(this));

        // Hook up the pay button to the card widget and stripe instance
        payButton = findViewById(R.id.payButton);
        payButton.setOnClickListener(this::payButtonOnClick);
    }

    private void payButtonOnClick(View view) {
        CardInputWidget cardInputWidget = findViewById(R.id.cardInputWidget);
        PaymentMethodCreateParams params = cardInputWidget.getPaymentMethodCreateParams();
        if (params != null) {
            ConfirmPaymentIntentParams confirmParams = ConfirmPaymentIntentParams
                    .createWithPaymentMethodCreateParams(params, paymentIntentClientSecret);
            stripe.confirmPayment(this, confirmParams);
        }
    }

    private void displayAlert(@NonNull String title, @Nullable String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message);

        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Handle the result of stripe.confirmPayment
        stripe.onPaymentResult(requestCode, data, new PaymentResultCallback(this));
    }

    private void onPaymentSuccess(@NonNull final Response response) throws IOException {
        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, String>>(){}.getType();
        Map<String, String> responseMap = gson.fromJson(
                Objects.requireNonNull(response.body()).string(),
                type
        );

        paymentIntentClientSecret = responseMap.get("clientSecret");
    }




    private static final class PaymentIntentCallback implements Callback {
        @NonNull private final WeakReference<PaymentActivity> activityRef;

        PaymentIntentCallback(@NonNull PaymentActivity activity) {
            activityRef = new WeakReference<>(activity);
        }

        @Override
        public void onFailure(@NonNull Call call, @NonNull IOException e) {
            final PaymentActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            Log.e("STRIPE", e.getMessage());
            activity.runOnUiThread(() ->
                    Toast.makeText(
                            activity, "Error: Intent onFailure" + e.toString(), Toast.LENGTH_LONG
                    ).show()
            );
        }

        @Override
        public void onResponse(@NonNull Call call, @NonNull final Response response)
                throws IOException {
            final PaymentActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            if (!response.isSuccessful()) {
                activity.runOnUiThread(() ->
                        Toast.makeText(
                                activity, "Error: Intent onResponse" + response.toString(), Toast.LENGTH_LONG
                        ).show()
                );
                Log.e("STRIPE", response.toString());

            } else {
                activity.onPaymentSuccess(response);
            }
        }
    }




    private static final class PaymentResultCallback
            implements ApiResultCallback<PaymentIntentResult> {
        @NonNull private final WeakReference<PaymentActivity> activityRef;

        DatabaseManager databaseManager = null;

        PaymentResultCallback(@NonNull PaymentActivity activity) {
            activityRef = new WeakReference<>(activity);
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onSuccess(@NonNull PaymentIntentResult result) {
            final PaymentActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            PaymentIntent paymentIntent = result.getIntent();
            PaymentIntent.Status status = paymentIntent.getStatus();
            if (status == PaymentIntent.Status.Succeeded) {
                // Payment completed successfully
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                activity.displayAlert("Payment completed", gson.toJson(paymentIntent));

                if(product != null) {
                    databaseManager = new DatabaseManager();
                    databaseManager.addPurchase(product);
                }

            }
            else if (status == PaymentIntent.Status.RequiresPaymentMethod) {
                // Payment failed – allow retrying using a different payment method
                activity.displayAlert( "Payment failed - RequiresPaymentMethod",  Objects.requireNonNull(paymentIntent.getLastPaymentError()).getMessage()
                );
            }
        }

        @Override
        public void onError(@NonNull Exception e) {
            final PaymentActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            // Payment request failed – allow retrying using the same payment method
            activity.displayAlert("Error", e.toString());
        }
    }
}