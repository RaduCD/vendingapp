package com.example.vendingapp.authentication;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vendingapp.R;
import com.example.vendingapp.home.HomeActivity;
import com.example.vendingapp.manager.DatabaseManager;
import com.example.vendingapp.utils.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Objects;

public class LoginFragment extends Fragment {

    private EditText email_ed;
    private EditText password_ed;

    private Button login_btn;
    private ProgressBar progressBar;

    private TextView register_tv;
    private TextView forgot_password_tv;

    private FirebaseAuth fireBaseAuth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_login, container, false);




        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);



        email_ed = view.findViewById(R.id.id_email_address_ed);
        password_ed = view.findViewById(R.id.id_password_ed);

        login_btn = view.findViewById(R.id.id_login_btn);
        progressBar = view.findViewById(R.id.id_progressBar_login);

        register_tv = view.findViewById(R.id.id_register_tv);
        forgot_password_tv = view.findViewById(R.id.id_forgot_password_tv);

        fireBaseAuth = FirebaseAuth.getInstance();

        setAllListeners();

        return view;
    }

    private void setAllListeners(){
        login_btn.setOnClickListener(this::loginUser);
        register_tv.setOnClickListener(this::goToRegisterFragment);
        //forgot_password_tv.setOnClickListener(this);
    }

    private void loginUser(View view){
        String email = email_ed.getText().toString().trim();
        String password = password_ed.getText().toString().trim();

        if(isValidFields(email, password)){
            progressBar.setVisibility(View.VISIBLE);
            fireBaseAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                FirebaseUser user = fireBaseAuth.getCurrentUser();
                                Toast.makeText(getContext(), "Authentication succeeded", Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                startActivity(intent);
                            }
                            else {
                                Toast.makeText(getContext(), "Authentication failed", Toast.LENGTH_SHORT).show();
                            }
                            progressBar.setVisibility(View.GONE);
                        }
                    });

        }
    }

    private boolean isValidFields(String email, String password){
        if(email.isEmpty()){
            email_ed.setError("Please enter your username");
            email_ed.requestFocus();
            return false;
        }

         if(password.isEmpty()){
            password_ed.setError("Please enter your password");
            password_ed.requestFocus();
            return false;
        }
         return true;
    }

    private void goToRegisterFragment(View view){
        RegisterFragment registerFragment = new RegisterFragment();
        Utils.goToFragmentFromFragment(Objects.requireNonNull(getActivity()), registerFragment, "REGISTER FRAGMENT");
    }

}