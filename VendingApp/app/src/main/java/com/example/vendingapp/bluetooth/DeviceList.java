package com.example.vendingapp.bluetooth;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vendingapp.R;
import com.example.vendingapp.manager.BluetoothManager;
import com.example.vendingapp.model.Product;
import com.example.vendingapp.payment.PaymentActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

public class DeviceList extends AppCompatActivity {

    private BluetoothManager bluetoothManager;
    private ProgressDialog progress;

    private ImageView bluetooth_logo;
    private Button paired_btn;
    private Button disconnect_btn;
    private ListView pairedDevices_lv;

    private BluetoothAdapter myBluetooth = null;
    private Set<BluetoothDevice> pairedDevices;
    public static String EXTRA_ADDRESS = "device_address";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);

        bluetoothManager = BluetoothManager.getInstance();

        paired_btn = (Button)findViewById(R.id.id_paired_devices_btn);
        disconnect_btn = (Button)findViewById(R.id.id_disconnect_btn);
        pairedDevices_lv = (ListView)findViewById(R.id.id_devices_lv);

        myBluetooth = BluetoothAdapter.getDefaultAdapter();

        if(myBluetooth == null) {
            Toast.makeText(getApplicationContext(), "Bluetooth device not available", Toast.LENGTH_LONG).show();
            finish();
        }
        else if(!myBluetooth.isEnabled()){
            Intent turnBtOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnBtOn,1);
        }

        paired_btn.setOnClickListener(this::pairedDevicesList);
        disconnect_btn.setOnClickListener(this::closeConnection);
    }


    private void pairedDevicesList(View view)
    {
        pairedDevices = myBluetooth.getBondedDevices();
        ArrayList list = new ArrayList();

        if (pairedDevices.size()>0) {
            for(BluetoothDevice bt : pairedDevices){
                list.add(bt.getName() + "\n" + bt.getAddress()); //Get the device's name and the address
            }
        }
        else{
            Toast.makeText(getApplicationContext(), "No Paired Bluetooth Devices Found.", Toast.LENGTH_LONG).show();
        }

        final ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        pairedDevices_lv.setAdapter(adapter);
        pairedDevices_lv.setOnItemClickListener(myListClickListener); //Method called when the device from the list is clicked

    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();
    }

    private void closeConnection(View view){

        try{
            bluetoothManager.Disconnect();
            msg("Connection closed.");
        }
        catch (IOException e) {
            msg("Error while closing connection.");
        }
        finish();
    }

    private AdapterView.OnItemClickListener myListClickListener = new AdapterView.OnItemClickListener()
    {
        public void onItemClick (AdapterView<?> av, View v, int arg2, long arg3)
        {
            // Get the device MAC address, the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            String address = info.substring(info.length() - 17);

            bluetoothManager.setAddress(address);

            new ConnectBT().execute();
        }
    };

    //todo de actualizat culoarea logo-ului in timp real daca se poate
    private void updateLogoColor(){

        Resources resources = getApplicationContext().getResources();
        int disconnectedColor = resources.getColor(R.color.design_default_color_error);
        int connectedColor = resources.getColor(R.color.blue);

        bluetooth_logo = (ImageView) findViewById(R.id.bluetoothLogo);
        bluetoothManager = BluetoothManager.getInstance();

        if (bluetoothManager.getConnected())
            bluetooth_logo.setColorFilter(connectedColor);
        else
            bluetooth_logo.setColorFilter(disconnectedColor);
    }

    private void msg(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    public class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private final BluetoothManager bluetoothManager;
        private boolean isAlreadyConnected = false;

        public ConnectBT(){
            this.bluetoothManager = BluetoothManager.getInstance();
        }

        @Override
        protected void onPreExecute()
        {
            progress = ProgressDialog.show(DeviceList.this, "Connecting...", "Please wait!");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices)
        {
            try{
                if(!bluetoothManager.getConnected()) {
                    bluetoothManager.Connect();
                }
            }
            catch (IOException e) {
                Log.d("BLUETOOTH", "Connection failed. Try again.");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            super.onPostExecute(result);

            if(bluetoothManager.getConnected()){
                msg("Connected.");
                Intent intent = new Intent(DeviceList.this, PaymentActivity.class);
                Product product = (Product) getIntent().getSerializableExtra("Product");
                intent.putExtra("Product", product);

                startActivity(intent);
            }
            else {
                msg("Connection failed. Try again.");
                finish();
            }
            progress.dismiss();
        }
    }
}

