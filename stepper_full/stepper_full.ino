
const int leftRightStepPin = 4;
const int leftRightDirPin = 5;

const int leftRightMs1Pin = 6;
const int leftRightMs2Pin = 7;
const int leftRightMs3Pin = 8;


const int upDownStepPin = 9;
const int upDownDirPin = 10;

const int upDownMs1Pin = 11;
const int upDownMs2Pin = 12;
const int upDownMs3Pin = 13;

void setup() {
 pinMode(upDownStepPin, OUTPUT);
 pinMode(upDownDirPin, OUTPUT);
 
 pinMode(leftRightStepPin, OUTPUT);
 pinMode(leftRightDirPin, OUTPUT);
 


 pinMode(upDownMs1Pin, OUTPUT);
 pinMode(upDownMs2Pin, OUTPUT);
 pinMode(upDownMs3Pin, OUTPUT);
 
 pinMode(leftRightMs1Pin, OUTPUT);
 pinMode(leftRightMs2Pin, OUTPUT);
 pinMode(leftRightMs3Pin, OUTPUT);
}

void loop() {

//down-up
  digitalWrite(upDownMs1Pin, HIGH);
  digitalWrite(upDownMs2Pin, HIGH);
  digitalWrite(upDownMs3Pin, HIGH);
  digitalWrite(upDownDirPin, HIGH);
  for(int i =0; i < 3200; i++)
  {
    digitalWrite(upDownStepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(upDownStepPin, LOW);
    delayMicroseconds(500);
  }

  delay(1000);



 //left-right
  digitalWrite(leftRightMs2Pin, HIGH);
  digitalWrite(leftRightDirPin, HIGH);
  for(int i =0; i < 3200; i++)
  {
    digitalWrite(leftRightStepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(leftRightStepPin, LOW);
    delayMicroseconds(500);
  }

  delay(1000);


 //right-lefT
  digitalWrite(leftRightDirPin, LOW);
  for(int i =0; i < 3200; i++)
  {
    digitalWrite(leftRightStepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(leftRightStepPin, LOW);
    delayMicroseconds(500);
  }
  digitalWrite(leftRightMs2Pin, LOW);
  
  delay(1000);


 //up-down
  digitalWrite(upDownDirPin, LOW);
  for(int i =0; i < 3200; i++)
  {
    digitalWrite(upDownStepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(upDownStepPin, LOW);
    delayMicroseconds(500);
  }
  digitalWrite(upDownMs1Pin, LOW);
  digitalWrite(upDownMs2Pin, LOW);
  digitalWrite(upDownMs3Pin, LOW);
  
  delay(2000);
}
